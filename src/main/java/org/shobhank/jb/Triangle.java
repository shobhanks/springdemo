package org.shobhank.jb;

import org.springframework.beans.BeansException;
import org.springframework.beans.factory.DisposableBean;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;

/**
 *
 * @author shsharma
 */
public class Triangle implements ApplicationContextAware,InitializingBean,DisposableBean,Shape{
	
	private ApplicationContext context;
	
	private int base;
	
	private int height;

	private Point a;
	private Point b;
	private Point c;
	public Point getA() {
		return a;
	}
	public void setA(Point a) {
		this.a = a;
	}
	public Point getB() {
		return b;
	}
	public void setB(Point b) {
		this.b = b;
	}
	public Point getC() {
		return c;
	}
	public void setC(Point c) {
		this.c = c;
	}
	private String type;
	Triangle(){
		
	}
	Triangle(int height,int base){
		this.height = height;
		this.base = base;
	}
	
	Triangle(String type){
		this.type = type;
	}
	
	public void draw(){
		System.out.println(getType() + " Triangle Drawn of base " + getBase() + " and of height " + getHeight());
		System.out.println("(" + getA().getX() + "," + getA().getY() + ")");
		System.out.println("(" + getB().getX() + "," + getB().getY() + ")");
		System.out.println("(" + getC().getX() + "," + getC().getY() + ")");
		Point origin = (Point)context.getBean("origin");
		if(origin!=null)
			System.out.println("Origin is (" + origin.getX() + "," + origin.getY() + ")");
	}
	
	public int getBase() {
		return base;
	}
	
	public int getHeight() {
		return height;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public void setApplicationContext(ApplicationContext context)
			throws BeansException {
		// TODO Auto-generated method stub
		this.context = context;
	}
	public void afterPropertiesSet() throws Exception {
		// TODO Auto-generated method stub
		System.out.println("Initiazing Triangle bean");
	}
	public void destroy() throws Exception {
		// TODO Auto-generated method stub
		System.out.println("Destroying Triangle bean");
	}
}
