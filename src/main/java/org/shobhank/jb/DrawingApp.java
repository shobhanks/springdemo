package org.shobhank.jb;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.AbstractApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 *
 * @author shsharma
 */
public class DrawingApp {

	public static void main(String[] args) {
//		BeanFactory factory = new XmlBeanFactory(new FileSystemResource("/Users/shsharma/Documents/pulsar/SpringInActionDemo/src/main/resources/spring.xml"));
//		Triangle tr = (Triangle)factory.getBean("triangle");
		AbstractApplicationContext context = new ClassPathXmlApplicationContext("spring.xml"); //for desktop application use AbstractApplicationContext so that it can be closed
		context.registerShutdownHook();
//		Triangle tr1 = (Triangle) context.getBean("triangle1");
//		tr1.draw();
//		Triangle tr2 = (Triangle) context.getBean("triangle2");
//		tr2.draw();
//		Triangle tr3 = (Triangle) context.getBean("triangle3");
//		tr3.draw();
		Shape s1 = (Shape) context.getBean("triangle4");
		s1.draw();
		Triangle tr5 = (Triangle) context.getBean("triangle5");
		tr5.draw();
		tr5.setType("Equilateral");
		Shape s2 = (Shape) context.getBean("triangle5");
		s2.draw();
		Shape s3 = (Shape)context.getBean("circle");
		s3.draw();
	}

}
