package org.shobhank.jb;
/**
 *
 * @author shsharma
 */
public interface Shape {
	public void draw();
}
